
db.inventory.insertMany([
		{
			"name": "Javascript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishers"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100,
			"publisher": "JS Publishing House"
		}
	]);

// $gt operator - Matches the values that are greater than a specified value.

//$gte operator - Matches the values that are greater than or equal to a specified value

/*
	Syntax: 
		$gt:
		db.collectionName.find({"field" : {$gt: value}})
		
		$gte:
		db.collectionName.find({"field" : {$gte: value}})

*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
});

db.inventory.find({
	"stocks": {
		$gte: 50
	}
});

// $lt operator - Matches the values that are less than a specified value.
//$lte operator - Matches the values that are less than or equal to a specified value

/*
	Syntax: 
		$lt:
		db.collectionName.find({"field" : {$lt: value}})
		
		$lte:
		db.collectionName.find({"field" : {$lte: value}})

*/

db.inventory.find({
	"stocks": {
		$lt: 50
	}
});

db.inventory.find({
	"stocks": {
		$lte: 50
	}
});

// $ne (not equal) Operator - Macthes all values that aree not equal to a specified value
/*
	Syntax:
		db.collectionName.find({"field" : {$ne: value}})
*/

db.inventory.find({
	"stocks": {
		$ne: 50
	}
});

// $eq - Matches values that are specified to a specified value

/*
	Syntax:
		db.collectionName.find({"field" : {$eq: value}})
*/

db.inventory.find({
	"stocks": {
		$eq: 50
	}
});

//$in Operator - Matches any of the values specified in an array
/*
	Syntax:
		db.collectionName.find({"field": {$in: [value1, value2]}})
*/

db.inventory.find({
	"price": {
		$in: [5000,10000,2500]
	}
});

// $nin Operator - Matches none of the values specified in an array

/*
	Syntax:
		db.collectionName.find({"field": {$nin: [value1, value2]}})
*/

db.inventory.find({
	"price": {
		$nin: [5000,10000,2500]
	}
});

// To create a range condition
db.inventory.find({
	"price": {$gt: 2000} && {$lt: 4000}
})

db.inventory.find({
	"price": {$lte: 4000}
});

db.inventory.find({
	"stocks" :
	{$in: [50,100]}
});

// Logical Query Operators
/*
	$or operator - Joins query clauses with a logical OR operators and return all documents that match the conditions of their clauses

	Syntax:
		db.collectionName.find({
			$or: [
				{"fieldA": "ValueA"},
				{"fieldB": "ValueB"}
			]
		})
*/

db.inventory.find({
	$or: [
		{"name": "HTML and CSS"},
		{"publisher": "JS Publishing House"}
	]
});

db.inventory.find({
	$or: [
		{"author": "James Doe"},
		{"price": {
			$lte: 5000
		}}
	]
});

/*
	$and operator - Joins the query clauses with a logical AND operator and return all documents that match the conditions of both clauses.

	Syntax:
		db.collectionName.find({
			$and: [
				{"fieldA": "ValueA"},
				{"fieldB": "ValueB"}
			]
		})
*/

db.inventory.find({
	$and: [
		{"stocks": {
			$ne : 50
		}},
		{"price": {
			$ne : 5000
		}}
	]
});

// Without AND Operator

db.inventory.find({
	"stocks": {
			$ne : 50
		},
		"price": {
			$ne : 5000
		}
});

// Field Projection
// Inclusion - Matches the document according to the given criteria and returns included field/s

/*
	Syntax:
		db.collectionName.find({criteria}, {"field": 1}) 
*/

db.inventory.find({
	"publisher": "JS Publishing House"
	},
	{
		"name": 1,
		"author": 1,
		"price": 1
	}
);

// Exclusion - Matches document/s with the given criteria and returns the fields that were not excluded.

/*
	Syntax:
		db.collectionName.find({criteria}, {"field": 0}) 
*/

db.inventory.find({
	"author": "Noah Jimenez"
	},
	{
		"stocks": 0,
		"price": 0
	}
);

// We can't combine inclusion and exclusion except when hiding the _id

db.inventory.find({
	"price": {
		$lte: 5000
		}
	},
	{
		"_id": 0,
		"name": 1,
		"author": 1
	}

);

// Evaluates Query Operator

/*
	$regex operator (regular expressions) - selects documents where values match a specified regular expression

	Syntax: 
		db.collectionName.find({"field": {$regex: 'pattern', $options: 'optionValue'}})
*/

// case sensitive
db.inventory.find({
	"author": {
		$regex: 'M'
	}
});

// case insensitive
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
});

//Multiple letters in the pattern 

db.inventory.find({
	"name": {
		$regex: 'dev',
		$options: '$i'
	}
});

db.inventory.find({
	$and: [
		{"name": {
			$regex: 'java',
			$options: '$i'
		}},
		{"price": {
			$gte : 5000
		}}
	]
});

// Can we use query operators in other CRUD operations like updateOne, updateMany, deleteOne and deleteMany?

// Yes
/*
	updateOne({criteria/query}, {$set})
	updateMany({criteria/query}, {$set})

	deleteOne({criteria/query})
	deleteMany({criteria/query})
*/

db.inventory.updateOne({
	"price": {
		$gte: 3000
	}},
	{
		$set: {
			"stocks": 10
		}
	}
)
